<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ohm's Law</title>
</head>
<body>
	<form method="POST" id="myForm">
	    
	    Voltage(V):
		<input type="number" name="voltage" id="voltage"> <br> <br>
		Power(P):
		<input type="number"  name="power" id="power"> <br> <br>
		Current(I):
		<input type="number"  name="current" id="current"> <br> <br>
		Resistance(R): 
		<input type="number" name="resistance" id="resistance"> <br> <br>
		<input type="submit" name="Calculate" value="Calculate" id="calculate">
		<input type="submit" name="Clear" value="Clear" id="clear">
	    <p id="ohm"> </p>
	    <p id="pow"> </p>
	     <p id="cur"> </p>
	    <p id="res"> </p>
	     <p id="volt"> </p>
	    
	    
	    <script
		  src="https://code.jquery.com/jquery-1.12.4.min.js"
		  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
		  crossorigin="anonymous"></script>	
		  	
		<script>
		
			$("#calculate").click(function(){
				var voltage= $("#voltage").val();
				var power= $("#power").val();
				var current= $("#current").val();
				var resistance= $("#resistance").val();
				var v = document.getElementById('voltage').value;
				var p = document.getElementById('power').value;
				var i = document.getElementById('current').value;
				var r = document.getElementById('resistance').value;
				
				if(v!=null && i!=null && p==="" && r===""){
					
						p= v*i; 
						r= v/i; 
						document.getElementById("pow").innerHTML = p;
						document.getElementById("res").innerHTML = r;
						document.getElementbyId("pow").value= p;
				}			
		   		
						
				else if(v!=null && r!=null && i==="" && p==="")
				{
					i= v/r;
					p= Math.pow(v, 2)/r;
					document.getElementById("cur").innerHTML = i;
					document.getElementById("pow").innerHTML = p;
				 }	
					
			  
				else if(p!=null && v!=null && i==="" && r==="")
				{
					
					i= p/v;
					r= Math.pow(v,2)/p;
				
					document.getElementById("cur").innerHTML = i;
					document.getElementById("res").innerHTML = r;
					
				}
				
				else if(i!=null && r!=null && v==="" && p==="")
				{
					v= i*r;
					p=Math.pow(i, 2) * r;
					document.getElementById("volt").innerHTML = v;
					document.getElementById("power").innerHTML = p;
					
				}
				else if(p!=null && i!=null && v==="" && r==="")
				{
					
					v=p/i;
					r=p/Math.pow(i,2);
					
					document.getElementById("volt").innerHTML = v;
					document.getElementById("res").innerHTML = r;
					
				}
				
				else if(p!=null && v!=null && i==="" && v==="")
				{
					i=Math.sqrt(p/r);
					v=Math.sqrt(p*r);
					
					document.getElementById("cur").innerHTML = i;
					document.getElementById("volt").innerHTML = v;
					
				}
				
				else 
				{
				document.getElementById("ohm").innerHTML = "Only 2 inputs needed";
				
				}
				
				
			
			});

			
				
			
				
				
			

		</script> 

</body>
</html>